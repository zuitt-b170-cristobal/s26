let http = require("http");
/*
	require function - directive/function used to load a particular node module; in this case, we're trying to load http module from node.js
		http mpdule - Hypertext Transfer Protocol; lets nodejs transfer data; a set of individual files that are needed to create a component (used to establish data transfer between apps)
*/

http.createServer(function(request, response){
	/*
		createServer() allows creation of http server that listens to requests on a specified port & gives response back to client; accepts a function that allows performing of task for the server
	*/
	response.writeHead(200, {"Content-Type": "text/plain"});
	//sets status code (200) and content-type
	response.end("Hello World")
}).listen(4000);

console.log("Your server is now running at port: 4000");

	/*
		response.writeHead - used to set a status code for the response; 200 status code is default for nodejs since this means that the response is successfully processes
		100 - info
		200 - successful response
		300 - redirection message
		400 - client side error; no permission/unauthorized/ cannot be seen
		500 - server side error; service unavailable/ maintenance

		response.end - signals end of the response process

		listen("4000") - server will be assigned to the specified port using this command
 	
		port - virtual point where network connections start & end; each port is specific to a certain process/server

		Content-Type - sets the type of content which will be displayed on the client

		ctrl + c - gitbash will terminate any process being done in the directory/local repo
 	*/