const http = require("http");

const port = 4000;

const server = http.createServer((request, response)=>{
	if (request.url === "/greeting") {
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Hello World");
		/*
			Practice - create 2 more uri's and let your users see a message "Welcome to _____ Page"

			use successful status code & plain text as content
		*/
	}else{
		response.writeHead(404, {"Content-Type": "text/plain"});
		response.end("Page not found");
	}
});

server.listen(port);

console.log(`Server now running at: ${port}`);


