const http = require("http");

const port = 4000;

const server = http.createServer((request, response)=>{
	if (request.url === "/home") {
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Welcome to Home Page");
		
	}else if (request.url === "/about") {
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Welcome to About Page");
		
	}else{
		response.writeHead(404, {"Content-Type": "text/plain"});
		response.end("Page not found");
	}
});

server.listen(port);

console.log(`Server now running at: ${port}`);
